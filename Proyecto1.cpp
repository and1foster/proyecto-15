#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <iterator>

using namespace std;

string leerNombre();
void imprimirInstrucciones();
bool hacerMovimientoUsuario(int *tableroAndy,int *tableroAndyComparacion, int numeros, int &log, string movimiento);
bool hacerMovimientoRandom(int *tableroAndy,int *tableroAndyComparacion, int numeros, int &log, string movimiento);
void imprimirTableroConFormato(int *mat, int numeros);
void tableroInicial(int *tableroAndy, int *tableroAndyComparacion, int cantMovs, int numeros);
string leerMovimiento();
void comparaRecord(int &record, int log);

int main(){

//! se lee el nombre del usuario y se da bienvenida

    string name=leerNombre();
    cout<<"Bienvenido "<<name<<"!"<<endl;

//! arreglos y variables

    int cantMovs=2; //!cantidad de movimientos que se realizaran para desacomodar el tablero
    int const numeros=16;//!tama�o del arreglo
    int tableroAndy[numeros]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};
    int tableroAndyComparacion[numeros]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};
    int log =0;//!Variable para ir contando el numero de movimientos realizados por el jugador
    string movimiento;
    int record=0;

//! instrucciones
    imprimirInstrucciones();

//! tablero acomodado
    imprimirTableroConFormato(tableroAndy , numeros);

//! tablero descomodado/inicio del juego
    cout<<"\n\nDesacomodando tablero...\n";
    tableroInicial(tableroAndy,tableroAndyComparacion,cantMovs,numeros);
    cout<<"\nInicio del juego...\n";
    imprimirTableroConFormato(tableroAndy ,numeros);

//!Ciclo del juego hasta ganar

    do{
    movimiento= leerMovimiento();
    hacerMovimientoUsuario(tableroAndy,tableroAndyComparacion,numeros,log,movimiento);
    }while(!equal(begin(tableroAndy),end(tableroAndy),begin(tableroAndyComparacion)));
        cout<<"\nFelicitaciones, ganaste. Hiciste "<<log<<" movimientos";

    comparaRecord(record,log);



}

void imprimirTableroConFormato(int *mat, int numeros){
    cout<<"**********************\n\n";
    for(int i = 0; i < 16; i++)
    {
        cout<<setw(5)<<mat[i];
        if(i==3||i==7||i==11)
            cout<<"\n"<<endl;
    }
         cout<<"\n\n**********************\n\n";
}

string leerNombre(){
    string name;
    cout<<"Introduzca el nombre del jugador"<<endl;
    getline(cin,name);
    if (name.empty()) {
    cout<<"Ups! Por favor ";
    leerNombre();
    }
    return name;

}


void tableroInicial(int *tableroAndy,int *tableroAndyComparacion, int cantMovs, int numeros){
    int log=0;
    int numeroRandom;//!Numero random de movimiento (a,s,d,w)
    srand(time(NULL));//!Para que cada vez que se ejecute el programa, salgan nuevos numeros random
    do{

            for(int j=0;j<numeros;j++){//!Recorre las posiciones del arreglo
                if(tableroAndy[j]==0){//!SI, encuentras un cero entonces:
                    numeroRandom=rand()%4;//!Genera un numero random del 0 al 3
                        switch(numeroRandom){
                         case 0:hacerMovimientoRandom(tableroAndy,tableroAndyComparacion,numeros,log,"w");break;
                         case 1:hacerMovimientoRandom(tableroAndy,tableroAndyComparacion,numeros,log,"a");break;
                         case 2:hacerMovimientoRandom(tableroAndy,tableroAndyComparacion,numeros,log,"s");break;
                         case 3:hacerMovimientoRandom(tableroAndy,tableroAndyComparacion,numeros,log,"d");break;
                        }
                }
            }

    }while(log<cantMovs);

    cout<<"Hice "<<log<<" movimientos";
}

string leerMovimiento(){
    string movimiento;//!Lee W,A,S,D para saber a donde se va a mover el jugador
    cout<<"\nHacia donde quiere moverse?\n ";
    cin>>movimiento;//!Leemos W,A,S,D
    if(
       movimiento=="a"||movimiento=="A"||
       movimiento=="s"||movimiento=="S"||
       movimiento=="d"||movimiento=="D"||
       movimiento=="w"||movimiento=="W")
        return movimiento;
    else{
        leerMovimiento();
    }
}

bool hacerMovimientoUsuario(int *tableroAndy,int *tableroAndyComparacion, int numeros, int &log, string movimiento){

    if(movimiento=="w"||movimiento=="W"){//!Si, es W
        for(int i=0;i<numeros;i++){//!Recorre el arreglo
            if(tableroAndy[i]==0){//!Encuentra el cero
                if(i==0||i==1||i==2||i==3){//!Si el espacio vacio es la primera fila, entonces no se puede mover hacia arriba
                    cout<<"No puedes moverte hacia arriba!\n"<<endl;
                    return false;

                }else{//!Si no, se intercambia por el numero que esta arriba
                    tableroAndy[i]=tableroAndy[i-4];//!Le restamos 4 que seria la posicion que esta arriba
                    tableroAndy[i-4]=0;//!Lo intercambiamos por cero
                    log+=1;
                    cout<<log;
                    imprimirTableroConFormato(tableroAndy ,numeros);
                    return true;
                }
            }
        }
    }
    if(movimiento=="a"||movimiento=="A"){
        for(int i=0;i<numeros;i++){
            if(tableroAndy[i]==0){
                if(i==0||i==4||i==8||i==12){
                    cout<<"No puedes moverte hacia la izquierda!\n"<<endl;
                    return false;
                }else{
                    tableroAndy[i]=tableroAndy[i-1];
                    tableroAndy[i-1]=0;
                    log++;
                    cout<<log;
                    imprimirTableroConFormato(tableroAndy ,numeros);
                    return true;
                }
            }
        }
    }
    if(movimiento=="s"||movimiento=="S"){
        for(int i=0;i<numeros;i++){
            if(tableroAndy[i]==0){
                if(i==12||i==13||i==14||i==15){
                    cout<<"No puedes moverte hacia abajo!\n"<<endl;
                    return false;
                }else{
                    tableroAndy[i]=tableroAndy[i+4];
                    tableroAndy[i+4]=0;
                    log++;
                    cout<<log;
                    imprimirTableroConFormato(tableroAndy ,numeros);
                    return true;
                }
            }
        }
    }
    if(movimiento=="d"||movimiento=="D"){
        for(int i=0;i<numeros;i++){
            if(tableroAndy[i]==0){
                if(i==3||i==7||i==11||i==15){
                    cout<<"No puedes moverte hacia la derecha!\n"<<endl;
                    return false;
                }else{
                    tableroAndy[i]=tableroAndy[i+1];
                    tableroAndy[i+1]=0;
                    log++;
                    cout<<log;
                    imprimirTableroConFormato(tableroAndy ,numeros);
                    return true;
                }
            }
        }
    }

}

bool hacerMovimientoRandom(int *tableroAndy,int *tableroAndyComparacion, int numeros, int &log, string movimiento){

   if(movimiento=="w"||movimiento=="W"){//!Si, es W
        for(int i=0;i<numeros;i++){//!Recorre el arreglo
            if(tableroAndy[i]==0){//!Encuentra el cero
                if(i==0||i==1||i==2||i==3){//!Si el espacio vacio es la primera fila, entonces no se puede mover hacia arriba
                    return false;

                }else{//!Si no, se intercambia por el numero que esta arriba
                    tableroAndy[i]=tableroAndy[i-4];//!Le restamos 4 que seria la posicion que esta arriba
                    tableroAndy[i-4]=0;//!Lo intercambiamos por cero
                    log++;
                    return true;
                }
            }
        }
    }
    if(movimiento=="a"||movimiento=="A"){
        for(int i=0;i<numeros;i++){
            if(tableroAndy[i]==0){
                if(i==0||i==4||i==8||i==12){
                    return false;
                }else{
                    tableroAndy[i]=tableroAndy[i-1];
                    tableroAndy[i-1]=0;
                    log++;
                    return true;
                }
            }
        }
    }
    if(movimiento=="s"||movimiento=="S"){
        for(int i=0;i<numeros;i++){
            if(tableroAndy[i]==0){
                if(i==12||i==13||i==14||i==15){
                    return false;
                }else{
                    tableroAndy[i]=tableroAndy[i+4];
                    tableroAndy[i+4]=0;
                    log++;
                    return true;
                }
            }
        }
    }
    if(movimiento=="d"||movimiento=="D"){
        for(int i=0;i<numeros;i++){
            if(tableroAndy[i]==0){
                if(i==3||i==7||i==11||i==15){
                    return false;
                }else{
                    tableroAndy[i]=tableroAndy[i+1];
                    tableroAndy[i+1]=0;
                    log++;
                    return true;
                }
            }
        }
    }


}


void imprimirInstrucciones(){
    cout<<endl<<"\nInstrucciones\n"<<endl<< "Pulse: W para mover el espacio hacia arriba"
    <<endl<<"       A para mover el espacio hacia la izquierda"
    <<endl<<"       S para mover el espacio hacia la abajo"
    <<endl<<"       D para mover el espacio hacia la derecha";
     cout<<"\n\n";
}

void comparaRecord(int &record, int log){
    if(record==0){
        record=log;
        cout<<"\nPrimer record registrado: "<<record;
    }else{
        if(log<record){
            record=log;
            cout<<"\nFelicitaciones, hiciste un nuevo record: "<<record;
        }
    }
}
