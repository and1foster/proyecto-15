#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <iterator>

using namespace std;

string leerNombre();
void imprimirInstrucciones();
void imprimirTableroConFormato(int *mat, int =16);
void modificarTablero(int *mat);

int main(){
    string name=leerNombre();
    const int numeros = 16;
    int tablero[numeros] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};
    int tableroAndyComparacion[numeros]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};
    imprimirTableroConFormato(tablero);
    modificarTablero(tablero);
    imprimirTableroConFormato(tablero);


}
void modificarTablero(int *mat){
    for(int i = 0; i < 16; i++)
    {
        mat[i]=0;
    }

}

void imprimirTableroConFormato(int *mat, int numeros){
    for(int i = 0; i < 16; i++)
    {
        cout<<setw(5)<<mat[i];
        if(i==3||i==7||i==11)
            cout<<"\n"<<endl;
    }
}



void imprimirInstrucciones(){
    cout<<endl<<"\nInstrucciones\n"<<endl<< "Pulse: W para mover el espacio hacia arriba"
    <<endl<<"       A para mover el espacio hacia la izquierda"
    <<endl<<"       S para mover el espacio hacia la abajo"
    <<endl<<"       D para mover el espacio hacia la derecha";
    cout<<"\n\nDesacomodando tablero...\n";
}
string leerNombre(){
    string name;
    cout<<"Introduzca el nombre del jugador"<<endl;
    getline(cin,name);
    if (name.empty()) {
    cout<<"Ups! Por favor ";
    leerNombre();
    }
    cout<<"Bienvenido "<<name<<"!"<<endl;

}

